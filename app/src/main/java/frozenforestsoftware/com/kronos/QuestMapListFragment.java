package frozenforestsoftware.com.kronos;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;

public class QuestMapListFragment extends Fragment {
    //the list of maps for the previously selected game

    DatabaseReference mapReference;
    ChildEventListener mMapListener;
    ArrayList<String> mMapImageURLList;
    QuestVersionListAdapter mImageAdapter;
    ListView mListView;
    ArrayList<String> mapQuestPathList;
    public static ArrayList<String> mapBackgroundStringList;
    public static ArrayList<String> mapHeaderStringList;
    //public ArrayList<String> mLeftActions;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //this is the list of maps for a game
        View v= inflater.inflate(R.layout.quest_map_list_fragment,container,false);


        createMapListener();
        mapBackgroundStringList = new ArrayList<>();
        mapHeaderStringList = new ArrayList<>();
        mMapImageURLList = new ArrayList<>();
        mapQuestPathList = new ArrayList<>();
        //mLeftActions = new ArrayList<>();
        mapReference = FirebaseHelper.mDatabase.getReference().child("versions/"+getArguments().getInt("mapPath"));
        mListView = v.findViewById(R.id.mapListView);


            mImageAdapter = new QuestVersionListAdapter(getContext(), R.layout.quest_version_row_layout,mMapImageURLList);
            mListView.setAdapter(mImageAdapter);


        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                createMapQuestView( getArguments().getInt("mapPath"),mapQuestPathList.get(position)
                        ,mapHeaderStringList.get(position),mapBackgroundStringList.get(position));
                Toast.makeText(getContext(),mapQuestPathList.get(position),Toast.LENGTH_SHORT).show();
                //Toast.makeText(getContext(),mLeftActions.get(position),Toast.LENGTH_SHORT).show();
            }
        });
       // if (mapQuestPathList != null){
         //
        //}





        return v;

    }


    public QuestMapListFragment(){

    }

    @Override
    public void onResume() {
        super.onResume();

        mapReference.addChildEventListener(mMapListener);
    }

    @Override
    public void onPause() {
        mImageAdapter.clear();
        super.onPause();

        mapReference.removeEventListener(mMapListener);
    }

    public void createMapListener(){
        mMapListener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                mImageAdapter.add((String)dataSnapshot.child("url").getValue());
                mapQuestPathList.add((String)dataSnapshot.getKey());
                mapHeaderStringList.add((String) dataSnapshot.child("header_url").getValue());
                mapBackgroundStringList.add((String)dataSnapshot.child("background_image_url").getValue());
               // mLeftActions.add((String)dataSnapshot.child("MainQuest").getChildren().iterator().next().);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };

    }

    public static Fragment newInstance(int mapPath){

        QuestMapListFragment newFrag = new QuestMapListFragment();
        Bundle myBundle = new Bundle();
        myBundle.putInt("mapPath",mapPath);
        newFrag.setArguments(myBundle);
        return newFrag;


    }

    public void createMapQuestView(int versionKey,String position,String mapHeader,String background){
        FragmentManager myManager = getFragmentManager();
       myManager.beginTransaction().replace(R.id.mapViewLayout,
               QuestMapView.createNewMapQuestView(versionKey,position,mapHeader,background)).addToBackStack(null).commit();


    }

}
