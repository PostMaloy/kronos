package frozenforestsoftware.com.kronos;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;

public class QuestFragment extends Fragment {

    private ListView mVersionListView;
     private ArrayList<String> mVersionArray;
     private QuestVersionListAdapter mImageListAdapter;

     private DatabaseReference mVersionReference ;
    private ChildEventListener mVersionListener;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v =  inflater.inflate(R.layout.fragment_quest, container,false);



        mVersionReference = FirebaseHelper.mDatabase.getReference().child("versions");
        mVersionListView = v.findViewById(R.id.versionListView);

        createVersionImageListener();

            mVersionArray = new ArrayList();


            mImageListAdapter = new QuestVersionListAdapter(getContext(), R.layout.quest_version_row_layout, mVersionArray);
            mVersionListView.setAdapter(mImageListAdapter);
            mVersionListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    createMapViewFragment(position);
                }
            });









        return v;
    }




    @Override
    public void onResume() {
        mImageListAdapter.clear();
        super.onResume();


        mVersionReference.addChildEventListener(mVersionListener);


    }

    @Override
    public void onPause() {

        mImageListAdapter.clear();
        super.onPause();

        mVersionReference.removeEventListener(mVersionListener);
    }

    public void createVersionImageListener(){

        mVersionListener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {



                String myVersion = (String) dataSnapshot.child("versionImage").getValue();

                mImageListAdapter.add("https://firebasestorage.googleapis.com/v0/b/cod17-10d38.appspot.com/o/v5maps-BlackOps1%2Fkino.png?alt=media&token=bfbcbfd4-7426-485c-b4a4-1b25f9170873");
                //Toast.makeText(getContext(), myVersion,Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };

    }

    public void createMapViewFragment(int pos){
        FragmentManager myManager = getFragmentManager();
        myManager.beginTransaction().replace(R.id.questLayout, QuestMapListFragment.newInstance(pos)).addToBackStack("quests").commit();

    }

}
