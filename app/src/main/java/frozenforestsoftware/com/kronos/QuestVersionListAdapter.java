package frozenforestsoftware.com.kronos;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

public class QuestVersionListAdapter extends ArrayAdapter<String> {

    Context mContext;

    public QuestVersionListAdapter(@NonNull Context context, int resource, @NonNull List<String> objects) {
        super(context, resource, objects);
        mContext = context;
    }



    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View row = convertView;
        MyViewHolder holder = null;
        if(row == null){
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            row= inflater.inflate(R.layout.quest_version_row_layout,parent,false);
            holder = new MyViewHolder(row);
            row.setTag(holder);

        }else{
            holder = (MyViewHolder) row.getTag();
        }

        String currentVersion = getItem(position);

        Glide.with(mContext).load(currentVersion).apply(new RequestOptions().fitCenter()).into(holder.versionImage);





        return row;

    }

    private class MyViewHolder {

        ImageView versionImage;


        MyViewHolder(View v) {
            versionImage = v.findViewById(R.id.versionRowImage);


        }

    }

    @Nullable
    @Override
    public String getItem(int position) {
        return super.getItem(position);
    }
}
