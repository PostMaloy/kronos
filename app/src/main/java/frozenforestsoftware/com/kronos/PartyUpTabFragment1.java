package frozenforestsoftware.com.kronos;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class PartyUpTabFragment1 extends android.support.v4.app.Fragment {

    ListView pcPosts;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.party_up_tab_fragment1,container,false);

        pcPosts= v.findViewById(R.id.pcPostView);
        pcPosts.setAdapter(PartyUpFragment.mPartyUpPostAdapter);
        //when the view is created the adapter is only populated when a tab gets changed.







       // Toast.makeText(getContext(),"hey frag 1",Toast.LENGTH_LONG).show();


        return v;
    }


    @Override
    public void onResume() {
        super.onResume();
        PartyUpFragment.mPartyUpPostAdapter.clear();
        // this is so the adapter populates when the fragment is first loaded
        if(PartyUpFragment.mPartyUpPostAdapter.isEmpty()){
            PartyUpFragment.setupAndPopulateListView(0);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //if(PartyUpFragment.mPartyUpPostAdapter.isEmpty()){
       //     PartyUpFragment.setupAndPopulateListView(0);
       // }
    }
}
