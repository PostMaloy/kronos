package frozenforestsoftware.com.kronos;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;

public class QuestMapView extends Fragment {

    private ViewPager mQuestViewPager;
    private static DatabaseReference mMapQuestsReference;
    public static DatabaseReference questsTitleReference;
    private static ChildEventListener mQuestsListener;
    ImageView headerImage;
    ArrayList<Quests> questList;
    public static QuestsListViewAdapter questAdapter;
    public static int versionNumber ;
    public static String mapNumber;
    public static String questType;





    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //this is the main fragment where the quests are viewed and navigated
        View v = inflater.inflate(R.layout.quest_map_view_fragment, container, false);
        versionNumber = getArguments().getInt("versionNumber");
        mapNumber = getArguments().getString("mapPosition");

       // mMapQuestsReference = FirebaseHelper.mDatabase.getReference().child("versions/"
        //        +versionNumber+"/"+mapNumber);

        questList = new ArrayList<>();
        questAdapter = new QuestsListViewAdapter(getContext(),R.layout.quest_item_row,questList);


        headerImage= v.findViewById(R.id.questHeaderImage);
        Glide.with(getContext()).load(getArguments().getString("mapHeader")).apply(new RequestOptions().fitCenter()).into(headerImage);

        mQuestViewPager = v.findViewById(R.id.questPager);
        TabLayout questsTabLayout = v.findViewById(R.id.questTabLayout);

        setupViewPagerAdapter(mQuestViewPager);
        questsTabLayout.setupWithViewPager(mQuestViewPager);

        populateQuestTitleListView("MainQuest",versionNumber,mapNumber);
        //Toast.makeText(getContext(),questAdapter.getItem(0).getQuestTitle(),Toast.LENGTH_SHORT).show();

        questsTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch(tab.getPosition()){
                    case 0:
                        populateQuestTitleListView("MainQuest", versionNumber,mapNumber);
                        //questType = "MainQuest";
                        break;
                    case 1:
                        populateQuestTitleListView("SideQuest", versionNumber,mapNumber);
                        //questType = "SideQuest";
                        break;
                    case 2:
                        populateQuestTitleListView("Buildables",versionNumber,mapNumber);
                        //questType = "Buildables";
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });




        return v;


    }

    @Override
    public void onResume() {
        super.onResume();
       // questsTitleReference.addChildEventListener(mQuestsListener);
    }

    @Override
    public void onPause() {
        super.onPause();
        questsTitleReference.removeEventListener(mQuestsListener);
    }

    public void setupViewPagerAdapter(ViewPager myPager){

        QuestsPagerAdapter questAdapter = new QuestsPagerAdapter(getFragmentManager());
       questAdapter.addQuestFragment(new QuestTabFragment(),"Main Quests");
       questAdapter.addQuestFragment(new QuestTabFragment(),"Side Quests");
        questAdapter.addQuestFragment(new QuestTabFragment(),"Buildables");
        myPager.setAdapter(questAdapter);

    }

    public static Fragment createNewMapQuestView(int versionNumber, String mapPosition,String mapHeader,String background){

        QuestMapView questMapView =new QuestMapView();
        Bundle myBundle = new Bundle();
        myBundle.putInt("versionNumber",versionNumber);
        myBundle.putString("mapPosition",mapPosition);
        myBundle.putString("mapHeader",mapHeader);
        myBundle.putString("backgroundUrl",background);
        questMapView.setArguments(myBundle);


        return questMapView;


    }



    public static void populateQuestTitleListView(String whichQuests, int version, String map){
        if(!questAdapter.isEmpty()){
            questAdapter.clear();
        }
        questType = whichQuests;
        questsTitleReference = FirebaseHelper.mDatabase.getReference().child("versions/"+version+"/"+map+"/"+whichQuests);

        mQuestsListener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Quests currentQuests = new Quests();
                System.out.println((String)dataSnapshot.getKey()+"\n \n \n \n \n \n");
                currentQuests.setQuestTitle(dataSnapshot.getKey());

                if(dataSnapshot.hasChild("right_action")){
                    currentQuests.setYoutubeLink((String)dataSnapshot.child("right_action").getValue());
                }
                if (dataSnapshot.hasChild("left_action")){
                    currentQuests.setLeftActionLink((String)dataSnapshot.child("left_action").getValue());
                }
                questAdapter.add(currentQuests);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        questsTitleReference.addChildEventListener(mQuestsListener);



    }



}
