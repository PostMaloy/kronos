package frozenforestsoftware.com.kronos;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;

public class SettingsFragment extends Fragment {
    ListView settingsList;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_settings, container,false);

        String[] settingItems = getResources().getStringArray(R.array.settingsArray);
        settingsList = v.findViewById(R.id.settingsListView);
        settingsList.setAdapter(new SettingsListAdapter(getContext(),R.layout.settings_item_row,settingItems));
        settingsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                createSettingsViewFragment(position);
            }
        });


        return v;
    }


    public void createSettingsViewFragment(int position){
         Fragment myFrag = null;


        switch (position){
            case 0:
                String message = "We are a community built and managed app, so community feedback is extremely important. Send us an email and we'll get back to you ASAP!";
                String email = "kronosapp.feedback@gmail.com";
                Intent i2 = new Intent(Intent.ACTION_SENDTO);
                String mailTo2 = "mailto:"+email;
                i2.setData(Uri.parse(mailTo2));
                try {
                    startActivity(i2);
                    Toast.makeText(getContext(),message,Toast.LENGTH_SHORT).show();
                }catch (ActivityNotFoundException e){
                    Toast.makeText(getContext(), "No apps found to send email",Toast.LENGTH_LONG).show();
                }
                break;


            case 1:
                String discord = "https://discord.gg/c7tYKNn";
                Uri discordPage = Uri.parse(discord);



                Intent discordIntent = new Intent(Intent.ACTION_VIEW, discordPage);
                if (discordIntent.resolveActivity(getContext().getPackageManager()) != null) {
                    getContext().startActivity(discordIntent);
                }else {
                    Toast.makeText(getContext(),"No Apps found to open URL",Toast.LENGTH_SHORT).show();
                }
                break;

            case 2:
                String url = "https://docs.google.com/document/d/1KDY25KEIw5omuLwt00t4sWEyiEjvkCo7L15oZu4Ae-c/edit#";
                Uri webpage = Uri.parse(url);

                if (!url.startsWith("http://") && !url.startsWith("https://")) {
                    webpage = Uri.parse("http://" + url);
                }

                Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
                if (intent.resolveActivity(getContext().getPackageManager()) != null) {
                    getContext().startActivity(intent);
                }else {
                    Toast.makeText(getContext(),"No Apps found to open URL",Toast.LENGTH_SHORT).show();
                }

            case 3:
                String privacy = "https://docs.google.com/document/d/1CI_G0_sIN46Q6gvcP__6ELbHcLpFCdXucN1dBryadEQ/edit#heading=h.z9u31mqzfc0h";
                Uri webpage2 = Uri.parse(privacy);

                if (!privacy.startsWith("http://") && !privacy.startsWith("https://")) {
                    webpage = Uri.parse("http://" + privacy);
                }

                Intent privacyIntent = new Intent(Intent.ACTION_VIEW, webpage2);
                if (privacyIntent.resolveActivity(getContext().getPackageManager()) != null) {
                    getContext().startActivity(privacyIntent);
                }else {
                    Toast.makeText(getContext(),"No Apps found to open URL",Toast.LENGTH_SHORT).show();
                }

            case 4:
                Intent i = new Intent(Intent.ACTION_SENDTO);
                String mailTo = "mailto:"+"kronosapp.legal@gmail.com";
                i.setData(Uri.parse(mailTo));
                try {
                    startActivity(i);
                }catch (ActivityNotFoundException e){
                    Toast.makeText(getContext(), "No apps found to send email",Toast.LENGTH_SHORT).show();
                }
                break;

            case 5:
                String patreon = "https://www.patreon.com/kronosapp";
                Uri patreonPage = Uri.parse(patreon);

                if (!patreon.startsWith("http://") && !patreon.startsWith("https://")) {
                    patreonPage = Uri.parse("http://" + patreon);
                }

                Intent patreonntent = new Intent(Intent.ACTION_VIEW, patreonPage);
                if (patreonntent.resolveActivity(getContext().getPackageManager()) != null) {
                    getContext().startActivity(patreonntent);
                    Toast.makeText(getContext(),"Your support means we can make bigger and better features!",Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(getContext(),"No Apps found to open URL",Toast.LENGTH_SHORT).show();
                }

            case 6:
                FirebaseAuth.getInstance().signOut();
                Toast.makeText(getContext(),"Signed Out",Toast.LENGTH_SHORT).show();


        }



    }

}
