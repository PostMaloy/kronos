package frozenforestsoftware.com.kronos;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class QuestTabFragment extends Fragment {

ListView questListView;
public static String myQuest;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //the tab within the mapview screen for viewing main quests
        View v = inflater.inflate(R.layout.quest_tab,container,false);

        questListView = v.findViewById(R.id.QuestListView);
        questListView.setAdapter(QuestMapView.questAdapter);

        questListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView questname = view.findViewById(R.id.QuestName);
                 myQuest = (String) questname.getText();
                createInspectFragment(QuestMapView.versionNumber+"/"+
                        QuestMapView.mapNumber+"/"+QuestMapView.questType);
            }
        });


        return v;
    }


    public void createInspectFragment(String pathToQuest){
        FragmentManager myManager = getFragmentManager();
        myManager.beginTransaction()
                .replace(R.id.questBackgroundLayout, QuestInspectingFragment.createInspectFragment(pathToQuest))
                .addToBackStack(null).commit();


    }
}
