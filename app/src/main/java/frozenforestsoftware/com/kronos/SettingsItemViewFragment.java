package frozenforestsoftware.com.kronos;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.auth.ui.email.EmailActivity;

public class SettingsItemViewFragment extends Fragment {

    TextView mFragmentText;
    ImageView mFragmentImage;
    Button mButton;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.settings_view_fragment,container,false);

        mFragmentText = v.findViewById(R.id.settingsFragText);
        mFragmentImage = v.findViewById(R.id.settingsImageView);
        mButton = v.findViewById(R.id.settingsActionButton);

        mFragmentText.setText(getArguments().getString("textBody"));
        Glide.with(getContext()).load(getArguments().getInt("image")).into(mFragmentImage);
        mButton.setText(getArguments().getString("buttonText"));
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createAppropriateIntent(getArguments().getInt("switchCase"));

        }
        });

        return v;


    }


    public static Fragment createSettingsFragment(String text, String link,String buttonText, int imageID,int position){
        SettingsItemViewFragment fragment = new SettingsItemViewFragment();
        Bundle myBundle = new Bundle();
        myBundle.putInt("switchCase",position);
        myBundle.putString("textBody",text);
        myBundle.putString("textLink",link);
        myBundle.putInt("image",imageID);
        myBundle.putString("buttonText", buttonText);
        fragment.setArguments(myBundle);
        return fragment;

    }

    public void createAppropriateIntent(int position){

        Intent i = null;
        switch (position){

            case 0:
                 i = new Intent(Intent.ACTION_SENDTO);
                 String mailTo = "mailto:"+getArguments().getString("textLink");
                 i.setData(Uri.parse(mailTo));
                try {
                    startActivity(i);
                }catch (ActivityNotFoundException e){
                    Toast.makeText(getContext(), "No apps found to send email",Toast.LENGTH_SHORT).show();
                }
                 break;

            case 1:
                //TODO connect with us
                break;

            case 2:
                break;
        }


    }
}
