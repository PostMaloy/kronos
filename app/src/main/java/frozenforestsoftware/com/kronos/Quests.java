package frozenforestsoftware.com.kronos;

public class Quests {

    String questTitle;
    String youtubeLink;
    String leftActionLink;

    public String getQuestTitle() {
        return questTitle;
    }

    public void setQuestTitle(String questTitle) {
        this.questTitle = questTitle;
    }

    public String getYoutubeLink() {
        return youtubeLink;
    }

    public void setYoutubeLink(String youtubeLink) {
        this.youtubeLink = youtubeLink;
    }

    public String getLeftActionLink() {
        return leftActionLink;
    }

    public void setLeftActionLink(String leftActionLink) {
        this.leftActionLink = leftActionLink;
    }
}
