package frozenforestsoftware.com.kronos;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;

public class PartyUpNewPostDialog extends DialogFragment {

    Button okButton;
    Button cancelButton;
    EditText usernameBox;
    EditText messageBox;
    Spinner mapSpinner;
    Spinner skillSpinner;
    CheckBox micBox;

    DatabaseReference postPath = FirebaseHelper.mDatabase.getReference().child("post/"+PartyUpFragment.pathForPost);




    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.party_up_create_post_dialog_fragment,null,false);
         okButton = view.findViewById(R.id.okButton);
         mapSpinner = view.findViewById(R.id.mapSpinner);
         skillSpinner = view.findViewById(R.id.skillSpinner);
         messageBox = view.findViewById(R.id.messageEdit);
         usernameBox = view.findViewById(R.id.usernameEdit);
         cancelButton = view.findViewById(R.id.cancelButton);
         micBox = view.findViewById(R.id.micBox);
         String[] skillLevels = {"Beginner","Average","Skilled"};

        ArrayAdapter<String> adapter2 = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item,skillLevels);

         ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, PartyUpPostAdapter.mSpinnerMaps);
         adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
         mapSpinner.setAdapter(adapter);
         skillSpinner.setAdapter(adapter2);
         Toast.makeText(getContext(),"Do not use explicit language within posts",Toast.LENGTH_LONG).show();


        // TODO if !null, allow message creation. check to ensure all fields have values



        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(checkAllFieldsForValue()){
                Toast.makeText(getContext(),"OK pressed",Toast.LENGTH_LONG).show();
                PartyUpPosts sendPost = createCurrentPost();
                postPath.push().setValue(sendPost);
                dismiss();
            }else {
                    Toast.makeText(getContext(),"You must complete all fields",Toast.LENGTH_LONG).show();
                }

            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen);

        return view;
    }


    private boolean checkAllFieldsForValue(){
        if(messageBox.length()!= 0 & usernameBox.length()!=0 ){

            return true;

        }else {

            return false;
        }
    }

    public PartyUpPosts createCurrentPost(){

        PartyUpPosts newPost = new PartyUpPosts(usernameBox.getText().toString(),
                skillSpinner.getSelectedItem().toString(), messageBox.getText().toString()
                , Integer.toString(mapSpinner.getSelectedItemPosition()+1),micBox.isChecked());
        return newPost;

    }






}
