package frozenforestsoftware.com.kronos;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class SettingsListAdapter extends ArrayAdapter<String> {
    Context settingsContext;
    String[] items;

    public SettingsListAdapter(@NonNull Context context, int resource, @NonNull String[] myList) {
        super(context, resource, myList);
        settingsContext = context;
        items = myList;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View row = convertView;
        MyViewHolder holder = null;
        if(row == null){
            LayoutInflater inflater = (LayoutInflater) settingsContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            row= inflater.inflate(R.layout.settings_item_row,parent,false);
            holder = new MyViewHolder(row);
            row.setTag(holder);

        }else{
            holder = (MyViewHolder) row.getTag();
        }
        holder.rowTextView.setText(items[position]);




        return row;
    }
    private class MyViewHolder{
        TextView rowTextView;
        MyViewHolder(View v){
            rowTextView = v.findViewById(R.id.settingsRowTextView);
        }

    }

    @Nullable
    @Override
    public String getItem(int position) {
        return super.getItem(position);
    }
}
