package frozenforestsoftware.com.kronos;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;

public class QuestInspectingFragment extends Fragment{
   // public static ArrayList<String> mParentStepsList;

    public ArrayList<QuestObject> mRealQuests;
    public ArrayList<QuestObject> mQuestObjectList;


    public ExpandableListView ExpListView;
    LinearLayout toolBarLayout;
    public ChildEventListener mStepsListener;
    public DatabaseReference mStepsReference ;
    String referencePath;
    public static QuestExpListAdapter questAdapter;
    public boolean hasQuest = true;
    DatabaseReference questsReference;
    String stepString;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.quest_inspect_fragment,container,false);
        referencePath = "versions/"+getArguments().getString("questPath");
        mStepsReference = FirebaseHelper.mDatabase.getReference().child(referencePath);
        questsReference  = FirebaseHelper.mDatabase.getReference().child(referencePath+"/"+QuestTabFragment.myQuest+"/Steps");
        ExpListView = v.findViewById(R.id.ExpandableList);
        mQuestObjectList = new ArrayList<>();
        questAdapter = new QuestExpListAdapter(getContext(),mQuestObjectList);
        if(((questsReference.getParent().getParent()).toString()).contains("Buildables")) {
            createQuestNoSteps();
        }else {
            createQuestWithStepsListener();
        }




                //createStepsListener();

                // Toast.makeText(getContext(),mQuestObjectList.get(1).mStepTitleList,Toast.LENGTH_SHORT).show();


                ExpListView.setAdapter(questAdapter);

        return v;
    }

    @Override
    public void onResume() {
        //mQuestObjectList = new ArrayList<>();

        super.onResume();


                questsReference.addChildEventListener(mStepsListener);



    }

    @Override
    public void onPause() {
        questAdapter.mQuestsList.clear();
        super.onPause();
        questsReference.removeEventListener(mStepsListener);


    }

    public static Fragment createInspectFragment(String pathToQuest){
        QuestInspectingFragment questInspectingFragment = new QuestInspectingFragment();
        Bundle myBundle = new Bundle();
        myBundle.putString("questPath",pathToQuest);

        questInspectingFragment.setArguments(myBundle);

        return questInspectingFragment;
    }

    public void createQuestWithStepsListener() {
        mStepsListener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                //Currently working with /questname/steps
                //Populate Step Items
                DataSnapshot dataSnapshot2 = dataSnapshot;
                    QuestObject mQuest = new QuestObject();
                if (dataSnapshot2.hasChild("title")) {

                    if (((String) dataSnapshot2.child("title").getValue()).equals(" ")) {
                        mQuest.setmStepTitleList((dataSnapshot.getKey()));
                    } else {
                        mQuest.setmStepTitleList((String) dataSnapshot2.child("title").getValue());
                    }
                } else {

                    mQuest.setmStepTitleList((dataSnapshot2.getKey()));
                }

                //Populate Substeps Items
                if (dataSnapshot2.hasChild("Substeps")) {

                    for (DataSnapshot childSnapshot : dataSnapshot2.child("Substeps").getChildren()) {

                        System.out.println((String) childSnapshot.getKey() + "\n \n \n \n \n \n");
                        if (childSnapshot.hasChild("title")) {
                            mQuest.mSubstepTitle.add((String) childSnapshot.child("title").getValue());
                            //mSubstepList.add((String)childSnapshot.child("title").getValue());
                        }

                        //populate subsubsteps
                        if (childSnapshot.hasChild("subsubstep")) {
                            QuestObject.SubsubstepList mList = new QuestObject.SubsubstepList();
                            for (DataSnapshot subsubstep : childSnapshot.child("subsubstep").getChildren()) {
                                mList.mclassSubstepList.add((String) subsubstep.child("title").getValue());
                                // mSubSubStepList.add((String) subsubstep.child("title").getValue());
                            }
                            mQuest.mSubSubstep.add(mList);

                        }
                    }

                }
                    mQuestObjectList.add(mQuest);
                    questAdapter.notifyDataSetChanged();







            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
    }

//had to be written to support Buildables having no Steps nodes
    public void createQuestNoSteps(){
        questsReference  = FirebaseHelper.mDatabase.getReference().child(referencePath+"/"+QuestTabFragment.myQuest);
        mStepsListener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                    Toast.makeText(getContext(),"SecondLoopEntered",Toast.LENGTH_SHORT).show();

                            if(dataSnapshot.getKey().contains("sub")){
                            QuestObject mQuest = new QuestObject();
                            if (dataSnapshot.hasChild("title")) {

                                mQuest.mStepTitleList = ((String) dataSnapshot.child("title").getValue());
                                //mSubstepList.add((String)childSnapshot.child("title").getValue());
                            }else {
                                mQuest.mStepTitleList = dataSnapshot.getKey();
                            }
                            if (dataSnapshot.hasChild("subsubstep")) {

                                for (DataSnapshot subsubstep : dataSnapshot.child("subsubstep").getChildren()) {
                                    mQuest.mSubstepTitle.add((String) subsubstep.child("title").getValue());
                                    // mSubSubStepList.add((String) subsubstep.child("title").getValue());
                                }


                            }

                            mQuestObjectList.add(mQuest);
                            questAdapter.notifyDataSetChanged();


                            }

                        for(DataSnapshot childSnap : dataSnapshot.getChildren()){

                                if(childSnap.getKey().contains("sub")){
                                    QuestObject mQuest = new QuestObject();
                                    mQuest.mStepTitleList = dataSnapshot.getKey();

                                    for (DataSnapshot subsubstep : dataSnapshot.child("subsubstep").getChildren()) {
                                        mQuest.mSubstepTitle.add((String) subsubstep.child("title").getValue());
                                        // mSubSubStepList.add((String) subsubstep.child("title").getValue());
                                    }

                                    mQuestObjectList.add(mQuest);
                                    questAdapter.notifyDataSetChanged();



                                }
                        }
            }






            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };

    }


    public void createStepsListener(){
        mStepsReference = FirebaseHelper.mDatabase.getReference().child(referencePath);

        mStepsReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChild("Steps")){
                    createQuestWithStepsListener();
                }else {
                    createQuestNoSteps();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });







    }

}
