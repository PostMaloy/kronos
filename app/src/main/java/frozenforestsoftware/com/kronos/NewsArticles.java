package frozenforestsoftware.com.kronos;

public class NewsArticles {

    private String title;
    private String subTitle;
    private String date;
    private String imagePreview;
    private String paragraphs;

    public String getParagraphs() {
        return paragraphs;
    }

    public void setParagraphs(String paragraphs) {
        this.paragraphs = paragraphs;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setImagePreview(String imagePreview) {
        this.imagePreview = imagePreview;
    }

    public String getTitle() {
        return title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public String getDate() {
        return date;
    }

    public String getImagePreview() {
        return imagePreview;
    }
}
