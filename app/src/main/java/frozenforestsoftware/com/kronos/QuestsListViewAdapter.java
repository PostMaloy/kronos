package frozenforestsoftware.com.kronos;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.net.URI;
import java.util.ArrayList;

public class QuestsListViewAdapter extends ArrayAdapter<Quests> {
    Context mContext;
    public QuestsListViewAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Quests> objects) {
        super(context, resource, objects);
        mContext = context;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View row = convertView;
        MyViewHolder holder = null;
        if(row == null){
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            row= inflater.inflate(R.layout.quest_item_row,parent,false);
            holder = new MyViewHolder(row);
            row.setTag(holder);

        }else{
            holder = (MyViewHolder) row.getTag();
        }
        final Quests currentQuest = getItem(position);
        holder.questTitle.setText(currentQuest.getQuestTitle());
        if(currentQuest.getLeftActionLink()==null){
            holder.leftAction.setVisibility(View.GONE);
        }
        if(currentQuest.getYoutubeLink()== null){
            holder.rightAction.setVisibility(View.GONE);
        }
        holder.rightAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createViewFromUrl(currentQuest.getYoutubeLink());
            }
        });
        holder.leftAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createViewFromUrl(currentQuest.getLeftActionLink());
            }
        });




        return row;
    }

    private class MyViewHolder{
        TextView questTitle;
        ImageView leftAction;
        ImageView rightAction;

         MyViewHolder(View v){

             questTitle = v.findViewById(R.id.QuestName);
             leftAction = v.findViewById(R.id.questLeftAction);
             rightAction = v.findViewById(R.id.questRightAction);



        }


    }
    public void createViewFromUrl(String url){
        Toast.makeText(getContext(),url,Toast.LENGTH_SHORT).show();
        Uri webpage = Uri.parse(url);

        if (!url.startsWith("http://") && !url.startsWith("https://")) {
            webpage = Uri.parse("http://" + url);
        }

        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        if (intent.resolveActivity(getContext().getPackageManager()) != null) {
            getContext().startActivity(intent);
        }
    }

    @Nullable
    @Override
    public Quests getItem(int position) {
        return super.getItem(position);
    }
}
