package frozenforestsoftware.com.kronos;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.util.Linkify;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

public class NewsArticleViewFragment extends Fragment {

    ImageView headerImageView;
    TextView headerTextView;
    TextView paragraphTextView;
    DatabaseReference mArticleToRead;
    ChildEventListener mArticleListener;
    LinearLayout mLinearLayout;

    //used for creating divider line
    static final int COLOR = Color.DKGRAY;
    static final int HEIGHT = 2;
    static final int VERTICAL_MARGIN = 10;
    static final int HORIZONTAL_MARGIN = 10;
    static final int TOP_MARGIN = VERTICAL_MARGIN;
    static final int BOTTOM_MARGIN = VERTICAL_MARGIN;
    static final int LEFT_MARGIN = HORIZONTAL_MARGIN;
    static final int RIGHT_MARGIN = HORIZONTAL_MARGIN;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.news_article_view_fragment,container,false);
        mLinearLayout = v.findViewById(R.id.articleLinearLayout);
        headerImageView = v.findViewById(R.id.articleViewFeaturedImage);

        //headerTextView=v.findViewById(R.id.articleViewFeaturedText);
        RequestOptions myOptions = new RequestOptions();
        myOptions.centerCrop();
        Glide.with(getContext()).load(getArguments().getString("imageurl")).apply(myOptions).into(headerImageView);
        mArticleToRead = FirebaseHelper.mDatabase.getReference().child("news/articles/" + getArguments().getInt("path")+"/paragraphs");

        createListener();

        //TODO change text color for certain phrases


        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        mArticleToRead.addChildEventListener(mArticleListener);
    }

    @Override
    public void onPause() {
        super.onPause();
        mArticleToRead.removeEventListener(mArticleListener);
    }

    public NewsArticleViewFragment(){

    }

    //intended to get called on listItemTapped to create fragment with news info

    public static Fragment newInstance(String imageUrl, int path){
        NewsArticleViewFragment myFrag = new NewsArticleViewFragment();
        Bundle myBundle = new Bundle();
        myBundle.putString("imageurl", imageUrl);
        myBundle.putInt("path", path);
        myFrag.setArguments(myBundle);


        return myFrag;
    }


    public void createListener(){

        mArticleListener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                if(dataSnapshot.hasChild("text")){
                    addNewTextView((String) "\n"+ dataSnapshot.child("text").getValue());
                    //Toast.makeText(getContext(),(String)dataSnapshot.child("text").getValue(),Toast.LENGTH_LONG).show();
                }else {


                }
                if (dataSnapshot.hasChild("img")){
                    addImageView((String)dataSnapshot.child("img").getValue());

                }else {


                }

                if(dataSnapshot.hasChild("link")){
                    addURLView((String)dataSnapshot.child("link").getValue()+"\n");
                }

                if(dataSnapshot.hasChild("div")){
                    View v = new View(getContext());
                    v.setBackgroundColor(COLOR);
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            HEIGHT
                    );
                    lp.topMargin = TOP_MARGIN;
                    lp.bottomMargin = BOTTOM_MARGIN;
                    lp.leftMargin = LEFT_MARGIN;
                    lp.rightMargin = RIGHT_MARGIN;
                    mLinearLayout.addView(v,lp);
                }




            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };


    }

    public void addNewTextView(String content){

        TextView myTextView = new TextView(getContext());
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.leftMargin = 20;
        lp.rightMargin = 20;
        myTextView.setLayoutParams(lp);
        myTextView.setTextColor(Color.WHITE);



        mLinearLayout.addView(myTextView);
        //set the text after adding it to the layout, else it wont display
        myTextView.setText(content);
        myTextView.append("\n");

    }

    public void addImageView(String imgURL){
        ImageView imgView = new ImageView(getContext());
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(250, 250);
        lp.gravity = Gravity.CENTER_HORIZONTAL;
        imgView.setLayoutParams(lp);

        mLinearLayout.addView(imgView);
        Glide.with(getContext()).load(imgURL).into(imgView);


    }

    public void addURLView(String link){
        TextView urlView = new TextView(getContext());
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.leftMargin = 20;
        lp.rightMargin = 20;

        urlView.setLayoutParams(lp);
        mLinearLayout.addView(urlView);

        urlView.setTextColor(Color.BLUE);


        urlView.setText(link);

        Linkify.addLinks(urlView,Linkify.ALL);

    }

}
