package frozenforestsoftware.com.kronos;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

public class QuestsPagerAdapter extends FragmentStatePagerAdapter {

    private final ArrayList<Fragment> mQuestFragmentList = new ArrayList();
    private final ArrayList<String> mQuestTitleList = new ArrayList<>();


    public void addQuestFragment( Fragment fragment, String title){
        mQuestTitleList.add(title);
        mQuestFragmentList.add(fragment);
    }

    public QuestsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return mQuestFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mQuestFragmentList.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mQuestTitleList.get(position);
    }
}
