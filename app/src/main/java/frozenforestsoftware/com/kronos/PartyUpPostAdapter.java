package frozenforestsoftware.com.kronos;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;
import java.util.List;

public class PartyUpPostAdapter extends ArrayAdapter<PartyUpPosts> {
    Context mContext;

    public String mImagePath;
    public List<PartyUpImageAndMapName> mMapData = new ArrayList<>();
    public DatabaseReference mapsReference = FirebaseHelper.mDatabase.getReference().child("androidmaps");
    public ChildEventListener mChildListener;
    public static List<String> mSpinnerMaps = new ArrayList<>();


    public PartyUpPostAdapter(@NonNull Context context, int resource, @NonNull List<PartyUpPosts> objects) {
        super(context, resource, objects);
        mContext = context;
        attachListener();
    }



    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View row = convertView;
        MyViewHolder holder = null;
        if(row == null){
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            row= inflater.inflate(R.layout.party_up_post_row_layout,parent,false);
            holder = new MyViewHolder(row);
            row.setTag(holder);

        }else{
            holder = (MyViewHolder) row.getTag();
        }


        PartyUpPosts currentPost = getItem(position);

        holder.username.setText(currentPost.getUsername());
        holder.skillLevel.setText(currentPost.getLevel());
        holder.userMessage.setText(currentPost.getMsg());
        if(currentPost.getMic() == true){
            holder.micIcon.setImageResource(R.drawable.mic);
        }else {
            holder.micIcon.setImageResource(R.drawable.no_mic);
        }
        //setmImagePath(currentPost.getMap());

       String urlToLoad = mMapData.get(Integer.parseInt(currentPost.getMap())-1).getImage();

       Glide.with(mContext).load(urlToLoad).into(holder.map);




        return row;
    }

    @Nullable
    @Override
    public PartyUpPosts getItem(int position) {
        return super.getItem(super.getCount() - position - 1);
    }

    private class MyViewHolder{
        ImageView map;
        ImageView micIcon;
        TextView username;
        TextView userMessage;
        TextView skillLevel;
        MyViewHolder(View v){
            map = v.findViewById(R.id.partyRowMap);
            micIcon = v.findViewById(R.id.partyRowMic);
            userMessage = v.findViewById(R.id.partyRowMessage);
            skillLevel = v.findViewById(R.id.partyRowSkill);
            username =v.findViewById(R.id.partyRowUsername);



        }


    }

    public void setmImagePath(String url){
        this.mImagePath = url;

    }
    //

    public void attachListener(){
        mChildListener= new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                PartyUpImageAndMapName currentChild =dataSnapshot.getValue(PartyUpImageAndMapName.class);
                //adds current database child to arraylist as an ImageandMapname Object
                mMapData.add(currentChild);
                //adds the map name String to the spinner maps arraylist
                mSpinnerMaps.add(currentChild.getMap());

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        mapsReference.addChildEventListener(mChildListener);

    }

    public String splitDataForUrl(String myString){
        String[] myStringsArray = myString.split("=",2);
        String thisMap = myStringsArray[0];
        String imageUrl = myStringsArray[1];
        return imageUrl;
    }





}

/*
    String imageURL = (String) articleSnapshot.child("image").getValue();
                    Glide.with(getContext()).load(imageURL).into(newsFeaturedImage);

                            String featuredText = (String) articleSnapshot.child("feature-lbl").getValue();
                            String titleLabel = (String) articleSnapshot.child("title-lbl").getValue();
                            String subtitleLabel = (String) articleSnapshot.child("subtitle-lbl").getValue();
                            String dateLabel =  (String) articleSnapshot.child("date-lbl").getValue();
                            System.out.println(featuredText+"\n" + "THIS IS THE NUMBER \n \n \n \n \n");

                            Spannable FEATURED = new SpannableString(featuredText);
                            FEATURED.setSpan(new ForegroundColorSpan(Color.YELLOW),0,FEATURED.length(),Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);*/