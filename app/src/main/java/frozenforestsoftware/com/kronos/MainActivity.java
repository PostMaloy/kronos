package frozenforestsoftware.com.kronos;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toolbar;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class MainActivity extends AppCompatActivity {

    private FirebaseDatabase mDatabaseRoot;

    private DatabaseReference mNewsArticleReference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Initialize Firebase References
        mDatabaseRoot = FirebaseDatabase.getInstance();

        mNewsArticleReference = mDatabaseRoot.getReference().child("news/articles");


        //Initialize and set listener on NavBar
        BottomNavigationView bottomNav = findViewById(R.id.bottom_navigation);
        bottomNav.setOnNavigationItemSelectedListener(new NavBarListener());


        getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer, new QuestFragment()).commit();

    }


    private class NavBarListener implements BottomNavigationView.OnNavigationItemSelectedListener{
        @Override
        //change the fragment view depending on what nav button is tapped
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment selectedFragment = null;
            switch (item.getItemId()){
                case R.id.nav_home:
                    selectedFragment = new QuestFragment();
                    break;
                case R.id.nav_news:
                    selectedFragment = new NewsFragment();
                    break;
                case R.id.nav_party:
                    selectedFragment = new PartyUpFragment();
                    break;
                case R.id.nav_settings:
                    selectedFragment = new SettingsFragment();
                    break;
            }
            getSupportFragmentManager().popBackStack( null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

            //replace the contents of the fragmentContainer with a new fragment
            getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer, selectedFragment).commit();



            return true;
        }
    }


}
