package frozenforestsoftware.com.kronos;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

public class NewsArticleAdapter extends ArrayAdapter<NewsArticles>{

    Context mContext;


    public NewsArticleAdapter(@NonNull Context context, int resource, @NonNull List<NewsArticles> objects) {
        super(context, resource, objects);
        mContext = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View row = convertView;
        MyViewHolder holder = null;
        if(row == null){
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            row= inflater.inflate(R.layout.news_row_layout,parent,false);
            holder = new MyViewHolder(row);
            row.setTag(holder);

        }else{
            holder = (MyViewHolder) row.getTag();
        }

        NewsArticles currentArticle = getItem(position);

        holder.dateText.setText(currentArticle.getDate());
        holder.subtitleText.setText(currentArticle.getSubTitle());
        holder.titleText.setText(currentArticle.getTitle());

        RequestOptions myOptions = new RequestOptions();
        myOptions.centerCrop();
        Glide.with(getContext()).load(currentArticle.getImagePreview()).apply(myOptions).into(holder.imagePreview);

        return row;

    }

    @Nullable
    @Override
    public NewsArticles getItem(int position) {
        //reverses the order
        return super.getItem(super.getCount() - position - 1);
    }

    private class MyViewHolder{

        ImageView imagePreview;
        TextView titleText;
        TextView subtitleText;
        TextView dateText;

        MyViewHolder(View v){
            imagePreview = v.findViewById(R.id.newsRowImage);
            titleText= v.findViewById(R.id.newsTitleLabel);
            subtitleText = v.findViewById(R.id.newsSubtitleLabel);
            dateText = v.findViewById(R.id.newsDateLabel);


        }



    }
}
