package frozenforestsoftware.com.kronos;

public class PartyUpPosts {
    //fields that will be used to create and send party up posts

    private String msg;
    private String level;
    private String username;
    private boolean mic;
    private String map;
    public PartyUpPosts(){

    }

    public PartyUpPosts(String username, String level, String msg, String map, boolean mic){
        this.username = username;
        this.level = level;
        this.msg = msg;
        this.map = map;
        this.mic = mic;
    }

    public String getMsg(){
        return this.msg;
    }
    public String getLevel(){
        return this.level;
    }
    public String getUsername(){
        return this.username;
    }
    public boolean getMic(){
        return this.mic;
    }
    public String getMap(){
        return this.map;
    }



}
