package frozenforestsoftware.com.kronos;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class QuestExpListAdapter extends BaseExpandableListAdapter {
    private Context mContext;
     ArrayList<QuestObject> mQuestsList;

    public QuestExpListAdapter(Context context,ArrayList<QuestObject> newQuestList){
        mContext = context;
       this.mQuestsList = newQuestList;

    }

    @Override
    public int getGroupCount() {
        return mQuestsList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groupPosition;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }


    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
       View row = convertView;
       MyViewHolder mvh = null;
        if(row == null){
           LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
           row = inflater.inflate(R.layout.quest_exp_row1,null);
           mvh = new MyViewHolder(row);
           row.setTag(mvh);
       }else {
            mvh = (MyViewHolder) row.getTag();
        }

            mvh.step.setText("Step " + (groupPosition + 1) + ": " + mQuestsList.get(groupPosition).mStepTitleList);
        ExpandableListView mdp = (ExpandableListView) parent;
        mdp.expandGroup(groupPosition);
        row.setPadding(0,10,0,10);
        if(mQuestsList.size() == 0||mQuestsList.get(groupPosition).mStepTitleList.isEmpty()){
            Toast.makeText(mContext,"No Quests Available - Check Attached Links",Toast.LENGTH_SHORT).show();
        }
        return row;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        SubstepExpandableList substepExpList = new SubstepExpandableList(mContext);
        substepExpList.setAdapter(new SubstepListAdapter(mContext, mQuestsList.get(groupPosition).mSubstepTitle, mQuestsList.get(groupPosition).mSubSubstep));
        try {
            if(mQuestsList.get(groupPosition).mSubSubstep.get(childPosition).mclassSubstepList.size()==0){

            }
        }catch (IndexOutOfBoundsException e){
            substepExpList.setGroupIndicator(null);

        }


        return substepExpList;

    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


    public class SubstepExpandableList extends ExpandableListView {
        public SubstepExpandableList(Context context) {
            super(context);
        }

        protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            //999999 is a size in pixels. ExpandableListView requires a maximum height in order to do measurement calculations.
            heightMeasureSpec = MeasureSpec.makeMeasureSpec(999999, MeasureSpec.AT_MOST);
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }
    public class SubstepListAdapter extends BaseExpandableListAdapter{
        ArrayList<String> myPassedList;
        ArrayList<QuestObject.SubsubstepList> mySecondList;
        private Context mmContext;
        public SubstepListAdapter(Context context, ArrayList<String> myPassedList,ArrayList<QuestObject.SubsubstepList> myList ){
            this.mmContext = context;
            this.myPassedList = myPassedList;
            this.mySecondList = myList;
        }

        @Override
        public int getGroupCount() {
            return myPassedList.size();
        }

        @Override
        public int getChildrenCount(int groupPosition) {

           try {
               return mySecondList.get(groupPosition).mclassSubstepList.size();
            }catch (IndexOutOfBoundsException e){
               Toast.makeText(mmContext,"No Further Steps",Toast.LENGTH_SHORT).show();
               return 0;
           }
            //else {
           //     Toast.makeText(mmContext,"No Further Steps",Toast.LENGTH_SHORT).show();
           //     return 1;
           // }

        }

        @Override
        public Object getGroup(int groupPosition) {
            return groupPosition;
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public long getGroupId(int groupPosition) {
            return 0;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            View row = convertView;
            MyViewHolder mvh = null;

            if(row == null){
                LayoutInflater inflater = (LayoutInflater) mmContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                row = inflater.inflate(R.layout.quest_exp_row2,null);
                mvh = new MyViewHolder(row);
                row.setTag(mvh);
            }else {
                mvh = (MyViewHolder) row.getTag();

            }

            mvh.substep.setText(myPassedList.get(groupPosition));
            row.setPadding(5,5,0,5);

            return row;


        }

        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            View row = convertView;
            MyViewHolder mvh = null;

            if(row == null){
                LayoutInflater inflater = (LayoutInflater) mmContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                row = inflater.inflate(R.layout.quest_exp_row3,null);
                mvh = new MyViewHolder(row);
                row.setTag(mvh);
            }else {
                mvh = (MyViewHolder) row.getTag();

            }
            if(mySecondList.get(groupPosition)!= null ) {
                if (mySecondList.get(groupPosition).mclassSubstepList.size() !=0){
                mvh.subsub.setText(mySecondList.get(groupPosition).mclassSubstepList.get(childPosition));
            }}
            return row;


        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return false;
        }
    }

    private class MyViewHolder{

        TextView step;
        TextView substep;
        TextView subsub;
        MyViewHolder(View v){

            step = v.findViewById(R.id.questRow1Text);
            substep = v.findViewById(R.id.questRow2Text);
            subsub =v.findViewById(R.id.questRow3Text);



        }


    }
}
