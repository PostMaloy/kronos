package frozenforestsoftware.com.kronos;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;
import java.util.List;

public class NewsFragment extends Fragment {
    //this is the main news fragment, showing the scrolling list
    //of articles and tapping on one creates a new fragment
    //ListView articleListView;
    ImageView newsFeaturedImage;
    TextView newsFeaturedTextView;
    private DatabaseReference newsFeaturedReference = FirebaseHelper.mDatabase.getReference().child("news/featured");
    private DatabaseReference newsArticleReference = FirebaseHelper.mDatabase.getReference().child("news/articles");
    private ChildEventListener newsFeaturedListener;
    private ChildEventListener newsArticleListener;
    private int featuredArticleID;
    public NewsArticleAdapter mNewsArticleAdapter;
    public List<NewsArticles> mNewsArticlesList;
    public ListView newsListView;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View row = inflater.inflate(R.layout.fragment_news, container,false);

        //articleListView = row.findViewById(R.id.newsListView);
        newsFeaturedImage = row.findViewById(R.id.newsViewFeaturedImage);
        newsFeaturedTextView = row.findViewById(R.id.newsViewFeaturedText);
        newsListView = row.findViewById(R.id.newsListView);

        createNewsListeners();


        mNewsArticlesList = new ArrayList<>();
        mNewsArticleAdapter = new NewsArticleAdapter(getContext(),R.layout.news_row_layout, mNewsArticlesList);
        newsListView.setAdapter(mNewsArticleAdapter);

        newsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                NewsArticles myArt = mNewsArticleAdapter.getItem(position);
                //fixes incorrect article text bug
                createAndReplaceFragment(myArt.getImagePreview(), mNewsArticleAdapter.getCount()-position);

                Toast.makeText(getContext(),"This cell was clicked" + position,Toast.LENGTH_LONG).show();
            }
        });






        return row;
    }
    //add and remove listeners to prevent memory leaks
    @Override
    public void onResume() {
        super.onResume();
        newsArticleReference.addChildEventListener(newsArticleListener);
        newsFeaturedReference.addChildEventListener(newsFeaturedListener);

        //populateFeaturedBox(featuredArticleID);

    }

    @Override
    public void onPause() {
        //fixed bug happening when navigating away from view
        mNewsArticleAdapter.clear();
        super.onPause();
        newsFeaturedReference.removeEventListener(newsFeaturedListener);
        newsArticleReference.removeEventListener(newsArticleListener);
    }


    private void createNewsListeners(){



        newsArticleListener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                //create an article object and add it to articleList

                NewsArticles currentArticle = new NewsArticles();

                String myString = (String)dataSnapshot.child("title-lbl").getValue();

                currentArticle.setTitle((myString));
                currentArticle.setDate((String)dataSnapshot.child("date-lbl").getValue());
                currentArticle.setSubTitle(((String)dataSnapshot.child("subtitle-lbl").getValue()));
                currentArticle.setImagePreview(((String)dataSnapshot.child("image").getValue()));

                mNewsArticleAdapter.add(currentArticle);

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };


        newsFeaturedListener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                 long currentArticleID = (long) dataSnapshot.getValue();
                 featuredArticleID = (int) currentArticleID;
                 populateFeaturedBox(featuredArticleID-1);

               //Toast.makeText(getContext(),""+ currentArticleID, Toast.LENGTH_LONG).show();
                System.out.println(currentArticleID+"\n" + "THIS IS THE NUMBER \n \n \n \n \n \n");

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };




    }


    private void populateFeaturedBox(int articleID){
    //only focus on populating featured box
        //get article from index to populate featured
        NewsArticles currentArticle = mNewsArticlesList.get(articleID);

        RequestOptions myoptions = new RequestOptions();
        myoptions.centerCrop();
        Glide.with(getContext()).load(currentArticle.getImagePreview()).apply(myoptions).into(newsFeaturedImage);

        String title = currentArticle.getTitle();
        String subtitle = currentArticle.getSubTitle();

        Spannable FEATURED = new SpannableString("FEATURED");
        FEATURED.setSpan(new ForegroundColorSpan(Color.YELLOW),0,FEATURED.length(),Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        Spannable TITLE = new SpannableString(title);
        TITLE.setSpan(new ForegroundColorSpan(Color.WHITE),0,TITLE.length(),Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        newsFeaturedTextView.setText(FEATURED);
        newsFeaturedTextView.append("\n\n"+TITLE+"\n");
       // newsFeaturedTextView.append(subtitle);





    }

    public void populateArticleList(){




    }

    public void createAndReplaceFragment(String url, int pos){
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction myTrans = fragmentManager.beginTransaction();
        myTrans.replace(R.id.newsLayout, NewsArticleViewFragment.newInstance(url,pos));
        myTrans.addToBackStack(null);
        myTrans.commit();



    }

}
