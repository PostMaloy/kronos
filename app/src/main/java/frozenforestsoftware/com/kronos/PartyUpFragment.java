package frozenforestsoftware.com.kronos;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.firebase.ui.auth.AuthUI;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PartyUpFragment extends Fragment  {

    //TODO change when method for list gets called. pager calls it even when fragment isnt in view,

    private ViewPager mViewPager;


    public static PartyUpPostAdapter mPartyUpPostAdapter;
    public static List<PartyUpPosts> mPartyUpPostsList;
    private FirebaseAuth mFirebaseAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private boolean okToPost;
    public static int pathForPost;


    //only god knows what the fuck is going on here, but a brief summary is that im trying to
    //reuse the PartyUpPostAdapter for each Party up list, and it works.
    //each fragment sets the adapter of its list view to the same one.

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d("PartyUp", "Party Up Main Fragment Started");
        View view = inflater.inflate(R.layout.fragment_partyup, container,false);

        mPartyUpPostsList = new ArrayList<>();
        mPartyUpPostAdapter = new PartyUpPostAdapter(getContext(),R.layout.party_up_post_row_layout, mPartyUpPostsList);


        setupAuth();

        mViewPager = view.findViewById(R.id.pager);
        setupViewPager(mViewPager);

        TabLayout tabLayout = view.findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(mViewPager);


        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            //Populate list view depending on tab selected


            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()){
                    case  0:

                        PartyUpFragment.setupAndPopulateListView(0);

                        break;

                    case 1:

                        PartyUpFragment.setupAndPopulateListView(1);

                        break;

                    case 2:

                        PartyUpFragment.setupAndPopulateListView(2);

                        break;




                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {


            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case  0:

                        PartyUpFragment.setupAndPopulateListView(0);
                      //  pathForPost = 0;
                        break;

                    case 1:

                        PartyUpFragment.setupAndPopulateListView(1);
                       // pathForPost = 1;
                        break;

                    case 2:

                        PartyUpFragment.setupAndPopulateListView(2);
                       // pathForPost = 2;

                        break;

                }


            }
        });


        FloatingActionButton fab = view.findViewById(R.id.partyupFAB);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(okToPost){
                    createPostDialog();
                }else {
                    Toast.makeText(getContext(),"You must login or create an account",Toast.LENGTH_LONG).show();
                }

            }
        });



        return view;
    }
    //considered correct practice to add listener in onResume, then remove it in onPause.
    @Override
    public void onResume() {
        super.onResume();
        //Toast.makeText(getContext(),"On RESUME Called",Toast.LENGTH_LONG).show();
        mFirebaseAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onPause() {
        super.onPause();
        mFirebaseAuth.removeAuthStateListener(mAuthListener);
       // Toast.makeText(getContext(),"On Pause Called",Toast.LENGTH_LONG).show();
    }

    private void setupViewPager(ViewPager viewPager){
        //add fragments to the adapter. done this way so we can add a new fragment in this section if necessary.

        PartyUpPagerAdapter adapter = new PartyUpPagerAdapter(getFragmentManager());
        adapter.addFragment(new PartyUpTabFragment1(),"PC");
        adapter.addFragment(new PartyUpTabFragment2(),"PS4");
        adapter.addFragment(new PartyUpTabFragment3(),"XBOX");




        viewPager.setAdapter(adapter);

    }


    private void createPostDialog(){
        FragmentManager manager = getFragmentManager();
        PartyUpNewPostDialog myDialog = new PartyUpNewPostDialog();
        myDialog.show(manager,"MydaologTAG");

    }

    private void setBackgroud(ImageView imageView){
        //Create settings for a glide loader to load an image as background of view with center crop
        RequestOptions myOptions = new RequestOptions();
        myOptions.centerCrop();

        Glide.with(getContext()).load(R.drawable.background).apply(myOptions).into(imageView);
    }

    private void setupAuth(){
        mFirebaseAuth = FirebaseAuth.getInstance();
        final List<AuthUI.IdpConfig> myProviders = Arrays.asList(
                new AuthUI.IdpConfig.EmailBuilder().build()
        );
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if(user !=null){
                    okToPost = true;
                    Toast.makeText(getContext(),"Signed In",Toast.LENGTH_LONG).show();
                }else {
                    okToPost = false;
                    startActivityForResult(AuthUI.getInstance().createSignInIntentBuilder()
                            .setAvailableProviders(myProviders).build(),200);

                }

            }
        };
    }

    public static void setupAndPopulateListView(int platform){

        mPartyUpPostAdapter.clear();
        DatabaseReference getPostsReference = FirebaseHelper.mDatabase.getReference().child("post/"+platform);
        pathForPost = platform;

        ChildEventListener mEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                PartyUpPosts postRead = dataSnapshot.getValue(PartyUpPosts.class);
                mPartyUpPostAdapter.add(postRead);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };


            getPostsReference.addChildEventListener(mEventListener);


        }








    }









